# comparison-elliptic

This repository contains example scripts for the paper 

A Comparison of Consistent Discretizations for Elliptic Poisson-Type Problems on Unstructured Polyhedral Grids

(Manuscript in preparation, 2019)

Øystein S. Klemetsdal
Olav Møyner
Xavier Raynaud
Knut-Andreas Lie

The examples are implemented using the MATLAB Rservoir simulation Toolbox (MRST, mrst.no). To run the examples, (1) Clone MRST from bitbucket: https://bitbucket.org/mrst/mrst-core/wiki/Home (2) Start MATLAB and run the MRST startup script (mrst-core/startup.m) (3) Navigate the the examples folder of this repository, and write 'addpath ../utils' in the command window.

Questions and feedback are most velcome, and can be directed: to oystein.klemetsdal@sintef.no