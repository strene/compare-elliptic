function [wellSols, states, reports] = simulateScheduleIncomp(state0, G, rock, fluid, schedule, varargin)

    opt = struct('pressureSolver', [], ...
                 'conserveFlux'  , false, ...
                 'savepath'      , []);
    
    opt = merge_options(opt, varargin{:});
    
    pressureSolver = opt.pressureSolver;
    if isempty(pressureSolver)
        pressureSolver = @(state, fluid, src, bc, W) ...
            incompTPFA(state, G, computeTrans(G, rock), fluid, 'src', src, 'bc', bc, 'wells', W);
    end

    dt = schedule.step.val;
    nt = numel(dt);
    states = cell(nt,1);
    
    state = state0;
    for tNo = 1:nt
        
        fprintf('Solving timestep %d of %d ... ', tNo, nt);
        tic;
        
        cNo = schedule.step.control(tNo);
        bc  = schedule.control(cNo).bc;
        src = schedule.control(cNo).src;
        W   = schedule.control(cNo).W;
    
        state = pressureSolver(state, fluid, src, bc, W);
        if opt.conserveFlux
            state = conserveFlux(state, G, rock, 'bc', bc, 'src', src, 'tol', 1e-16);
        end
        state = implicitTransport(state, G, dt(tNo), rock, fluid, 'src', src, 'bc', bc, 'wells', W);
        states{tNo} = state;
        
        toc
        
        if ~isempty(opt.savepath)
            save(fullfile(opt.savepath, ['state_', num2str(tNo), '.mat']), 'state');
        end
        
    end
    
    wellSols = []; reports = [];
    
end