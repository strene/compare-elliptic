function G = alignedGrid(G, t)
    
    xmax = max(G.nodes.coords, [], 1);
    xmin = min(G.nodes.coords, [], 1);
    
    dx = xmax(1)-xmin(1);
    
    z  = dx./cos(t);
    zn = dx*(1-tan(t));
    yn = zn*sin(t);
    dxn = z+yn;
    
    x = linspace(xmin(1), xmin(1) + z+yn, G.cartDims(1)*dxn/dx);
    
    dy = xmax(2)-xmin(1);
    
    yn = dx*sin(t);
    xn = dy*cos(t);

    y = linspace(xmin(2)-yn, xmin(2) +xn, G.cartDims(2)*(yn+xn)/dy);

    [xx,yy] = ndgrid(x,y);
    
    x = [xx(:), yy(:)];
    
    R = [cos(t), -sin(t); sin(t), cos(t)];
    x = x*R';
    
    bdr = [xmin(1), xmin(2); ...
           xmax(1), xmin(2); ...
           xmax(1), xmax(2); ...
           xmin(1), xmax(2)];
    
    keep = inpolygon(x(:,1), x(:,2), bdr(:,1), bdr(:,2));
    
    x = x(keep,:);
    
    G = clippedPebi2D(x, bdr);
    
end