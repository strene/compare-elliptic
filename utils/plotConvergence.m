function h = plotConvergence(enorms, h, solverNames, slopes, varargin)

opt = struct('pos', [-1000, 0, 600, 500], ...
             'dy' , 2, ...
             'dofs', false);
opt = merge_options(opt, varargin{:});

clr = lines(numel(solverNames));

msz = linspace(4,18,6);
msz = msz(end:-1:1);
% figure('position', opt.pos);

hold on

if size(h,2) == 1
    h = repmat(h, 1, numel(solverNames));
end
slope = cell(1,numel(solverNames));
for sNo = 1:numel(solverNames)
    hh = h(:,sNo);
    e = enorms(:,sNo);
    if opt.dofs
        e = e;
    else
        e = e.*hh;
    end
    plot(hh, e, '-s', 'linew', 2, 'markerSize', msz(sNo), 'color', clr(sNo,:), 'markerFaceColor', clr(sNo,:));
    sl = polyfit(log(hh), log(e),1);
    slope{sNo} = round(sl(1)*100)/100;
end
    
ymin = min(enorms(1,:).*h(1));
ymax = max(enorms(1,:).*h(1));


s = slopes(1);

y = ymax*1.5;
f = @(h) h.^s*y/h(1)^s;

hh = h(:,end);

if ~opt.dofs
    plot(hh, f(hh), '--k')
    xx = sqrt(hh(2)*hh(3));
    hl2 = text(xx, f(xx)*0.6, ['Slope = ', num2str(slopes(1))]);
    hl2.Rotation = 15;

    if slopes(2) > 0
        s = slopes(2);
        y = ymin/2;
        f = @(h) h.^s*y/h(1)^s;
        plot(hh, f(hh), '--k')
        xx = sqrt(hh(2)*hh(3));
        hl3 = text(xx, f(xx)/1.3, ['Slope = ', num2str(slopes(2))]);
        hl3.Rotation = 20;
    end
end

hold off

if opt.dofs
    ymin = min(min(enorms));
    ymax = max(max(enorms));
else
    ymin = min(min(enorms.*h));
    ymax = max(max(enorms.*h));
end

if opt.dofs
    hmin = min(min(h));
    hmax = max(max(h));
    xlim([hmin - hmin/10, hmax + hmax/10]);
else
    xlim([hh(end) - hh(end)/10, hh(1) + hh(1)/10]);
end

ylim([ymin/opt.dy, ymax*opt.dy])

if opt.dofs
    xlabel('Number of dofs');
else
    xlabel('Mean cell diameter');
end
    
ylabel('Error');

ll = cellfun(@(sn,ss) [sn, ' (',num2str(ss),')'], solverNames, slope, 'unif', false);
% legend(ll, 'location', 'southoutside', 'orientation', 'horizontal')

legend(ll, 'location', 'southeast')
set(gca, 'yscale', 'log', 'xscale', 'log')
box on
ax = gca;
ax.FontSize = 15;
xx = round(hh*1e3)/1e3;
if ~opt.dofs
    xx = xx(end:-1:1);
end
ax.XTick = xx;
if opt.dofs
    ax.XDir = 'reverse';
end

end