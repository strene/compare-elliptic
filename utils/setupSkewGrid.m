function [G, blocks] = setupSkewGrid(n, theta, varargin)

    opt = struct('cart', false);
    opt = merge_options(opt, varargin{:});

    %% Set up grid

    bdr   = [-1,-1; 1,-1; 1, 1; -1 1]*0.5;
    fun   = @(y) -tan(theta-pi/2)*y;
    f     = {[-0.5 0; 0.5, 0], [fun(0.5), 0.5; fun(-0.5), -0.5]};

    if opt.cart
        nn = round(sqrt(n));
        if rem(nn,2) > 0
            nn = nn +1;
        end
        G = computeVEMGeometry(cartGrid([nn, nn], [1,1]));

        psi = @(x) [(1-x(:,1)).*(1-x(:,2)), ...
                    (x(:,1)).*(1-x(:,2)), ...
                    (x(:,1)).*(x(:,2)), ...
                    (1-x(:,1)).*(x(:,2))];

        xh2x = @(x,bdr) psi(x)*bdr;

        x0 = G.nodes.coords;

        bdr = [-0.5,-0.5; fun(-0.5), -0.5; fun(0.5), 0.5; -0.5, 0.5];
        left = x0(:,1) <= 0.5;
        G.nodes.coords(left,:) = xh2x(x0(left,:).*[2,1], bdr);

        bdr = [fun(-0.5), -0.5; 0.5, -0.5; 0.5,0.5; fun(0.5), 0.5];
        right = x0(:,1) > 0.5;
        G.nodes.coords(right,:) = xh2x((x0(right,:)-[0.5,0]).*[2,1], bdr);

        G = computeVEMGeometry(G);
    else
        d     = 1/sqrt(n);
        G     = computeVEMGeometry(compositePebiGrid([d,d], [1,1], 'faultLines', f,'polyBdr', bdr));
    end


    blocks = cell(4,1);
    reg{1} = @(x) x(:,1) >  fun(x(:,2)) & x(:,2) >= 0;
    reg{2} = @(x) x(:,1) <= fun(x(:,2)) & x(:,2) >= 0;
    reg{3} = @(x) x(:,1) <  fun(x(:,2)) & x(:,2) <  0;
    reg{4} = @(x) x(:,1) >= fun(x(:,2)) & x(:,2) <  0;

    for rNo = 1:4
        blocks{rNo}.c = reg{rNo}(G.cells.centroids);
        blocks{rNo}.f = reg{rNo}(G.faces.centroids);
        blocks{rNo}.n = reg{rNo}(G.nodes.coords);
    end

end
