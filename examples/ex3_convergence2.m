%% Example 3.2: Convergence
% In this example, we look at convergence properies in an example from
% Eigestad and Klaussen (2005, doi: 10.1002/num.20079)
close all
mrstModule add upr                                                    % Generate PEBI grids
mrstModule add incomp mpfa mimetic vem vemmech                        % Incompressible solvers
mrstModule add ad-core ad-props ad-blackoil blackoil-sequential ntpfa % Nonlinear TPFA (requires AD)

%% Generate grids
% The problem considers an analytical solution which is discontinuous
% across the line y = 0.5 and a line with slope $2\pi/3$ through the
% origin. We use setupSkewGrid to generate a Carteisan grid that conforms
% to the lines, and a composite PEBI grid that is Cartesian away from the
% two discontinuous lines.
theta = 2*pi/3;
Gc    = @(n) setupSkewGrid(n, theta, 'cart', true);
Gp    = @(n) setupSkewGrid(n, theta, 'cart', false);
% Names for plotting
gridNames = {'Cartesian', 'PEBI'};

%% Plot grid examples
figure('Position', [0, 0, 800, 400]);
colors = lines(4);
[gc, regc] = Gc(100);
[gp, regp] = Gp(100);
% Plot composite PEBI grid
subplot(1,2,1);
for i = 1:4
    plotGrid(gp, regp{i}.c, 'faceColor', colors(i,:)); axis equal tight
end
% Plot Cartesian grid
subplot(1,2,2);
for i = 1:4
    plotGrid(gc, regc{i}.c, 'faceColor', colors(i,:)); axis equal tight
end

%% Define analytical solution
% The analytical solution is discontinuous across the internal coundaries,
% and different in each of the regions (blue, red, yellow, purple)
alpha = 0.51671199;
a  = [ 1.0       ,  3.65306122, 0.65257532,-0.61658833];
b  = [ 0.60092521, -0.80940947,-0.96708664,-3.69050387];
% Solution in radial coordinates (+ 2 to avoid negative solutions)
pr = @(i, r, theta) r.^alpha.*(a(i)*cos(alpha*theta) + b(i)*sin(alpha*theta)) + 2;
% Solution in Cartesian coordinates
atan3 = @(x) atan2(x(:,2), x(:,1)) + (atan2(x(:,2), x(:,1)) < 0)*2*pi;
px    = @(i, x) pr(i, sqrt(sum(x.^2,2)), atan3(x));

%% Plot the analytical solution
figure('Position', [0, 0, 800, 300]);
pc = zeros(gc.nodes.num,1);
pp = zeros(gp.nodes.num,1);
% x     = cellfun(@(b) gc.nodes.coords(b.n,:), regc, 'uniformOutput', false);
% r     = cellfun(@(x) sqrt(sum(x.^2,2)), x, 'uniformOutput', false);
% theta = cellfun(@(x) atan3(x), x, 'uniformOutput', false);
for i = 1:4
    pc(regc{i}.n) = px(i, gc.nodes.coords(regc{i}.n,:));
    pp(regp{i}.n) = px(i, gp.nodes.coords(regp{i}.n,:));
end
subplot(1,2,1); unstructuredCarpetPlot(gc, pc, 'nodeval', true); view(3); axis tight
subplot(1,2,2); unstructuredCarpetPlot(gp, pp, 'nodeval', true); view(3); axis tight

%% Set up solvers
% We will use TPFA, MPFA, MFD (mimetic), 1st- and 2nd-order VEM, and NTPFA
% to solve the problem. We define them compactly as a cell array of
% function handles
fluid = initSingleFluid('mu', 1, 'rho', 1); % Mandatory
solvers = {
    @(state0, G, rock, bc) ...
    incompTPFA(state0, G, computeTrans(G, rock), fluid, ...
               'bc', bc, 'matrixOutput', true);
    @(state0, G, rock, bc) ...
    incompSinglePhaseNTPFA(PressureOilWaterModelNTPFA(G, rock, [], 'incTolPressure', 1e-14), 'bc', bc);
    @(state0, G, rock, bc) ...
    incompMPFA(state0, G, computeMultiPointTrans(G, rock), fluid, ...
               'bc', bc, 'matrixOutput', true);
    @(state0, G, rock, bc) ...
    incompMimetic(state0, G, computeMimeticIP(G, rock), fluid, ...
                  'bc', bc, 'matrixOutput', true);
    @(state0, G, rock, bc) ...
    incompVEM(state0, G, computeVirtualIP(G, rock, 1), fluid, ...
              'bc', bc, 'cellPressure', true, 'matrixOutput', true);
    @(state0, G, rock, bc) ...
    incompVEM(state0, G, computeVirtualIP(G, rock, 2), fluid, ...
              'bc', bc, 'cellPressure', true, 'matrixOutput', true);
};
% Names for plotting
solverNames = {'TPFA', 'NTPFA', 'MPFA', 'MFD', 'VEM1', 'VEM2'};
ns          = numel(solvers);

%% Solve problem on increasingly finer grids
% We investigate the convergence by solving the problem on successively
% finer grids. Tou can switch between Cartesian and PEBI grids below.
grid = 'PEBI'; % Either 'PEBI' or 'Cart'
nCells = [40, 80, 160, 360];   % Number of cells for different resolutions
ng     = numel(nCells);
states              = cell(ng,ns);
[h, nDof]           = deal(zeros(ng,1));
[errorL2, errorInf] = deal(zeros(ng,ns));
for gNo = 1:ng
    switch grid
        case 'PEBI'
            [G, reg] = Gp(nCells(gNo));
        case 'Cart'
            [G, reg] = Gc(nCells(gNo));
    end
    G         = computeVEMGeometry(G); % Compute geometry
    faces     = false(G.faces.num,1);  % Logical mask for boundary faces
    faces(boundaryFaces(G)) = true;
    
    % Define rock. Perm is 1 in region 2 and 4, and 6 in region 1 and 3
    rock      = makeRock(G, 1, 1);
    rock.perm(reg{1}.c | reg{3}.c) = rock.perm(reg{1}.c | reg{3}.c)*6;
    state0    = initResSol(G, 1, 1);        % Define initial state
    h(gNo)    = mean(G.cells.diameters);    % Store mean cell diameter
    for sNo = 1:ns
        fprintf('Solving %s on Cartesian grid with %d cells\n', ...
                solverNames{sNo}, G.cells.num);
        % Assign BCs (VEM can take function handle as BC)
        bc = [];
        for i = [2,3,4,1] % Ensure that we don't overwrite BCs in VEM
            pbc = @(x) px(i,x);
            f   = find(faces & reg{i}.f);
            if any(strcmpi(solverNames{sNo}, {'VEM1', 'VEM2'}))
                bc = addBCVEM(bc, f, 'pressure', @(x) px(i,x));
            else
                bc = addBC(bc, f, 'pressure', px(i, G.faces.centroids(f,:)));
            end
        end
        states{gNo, sNo} = solvers{sNo}(state0, G, rock, bc); % Compute solution
        nDof(gNo, sNo)   = G.cells.num; % Store nDof
        if isfield(states{gNo, sNo}, 'A')
            nDof(gNo,sNo) = size(states{gNo, sNo}.A, 1);
        end
        % Compute error norm and number of dofs
        if any(strcmpi(solverNames{sNo}, {'VEM1', 'VEM2'}))
            % VEM does has node pressure as dof, so we use this
            p = nan(G.nodes.num,1);
            tol = 1e-4;
            for i = [1,3,2,4]
                p(reg{i}.n) = px(i, G.nodes.coords(reg{i}.n,:));
            end
            errorL2(gNo, sNo)  = norm(states{gNo, sNo}.nodePressure - p);
            errorInf(gNo, sNo) = norm(states{gNo, sNo}.nodePressure - p, 'inf');
        else
            p = zeros(G.cells.num,1);
            for i = 1:4
                p(reg{i}.c) = px(i, G.cells.centroids(reg{i}.c,:));
            end
            errorL2(gNo, sNo)  = norm(states{gNo, sNo}.pressure - p);
            errorInf(gNo, sNo) = norm(states{gNo, sNo}.pressure - p, 'inf');
        end
    end

end

%% Plot convergence
% We investigate convergence by log-log-plots of L2 error vs mean cell
% diameter
figure(), plotConvergence(errorL2, h, solverNames, [1,2]);
title('L2 error vs mean cell diameter');
% ... and Linf error vs number of degrees of freedom
figure(), plotConvergence(errorInf, nDof, solverNames, [1,2], 'dofs', true);
title('Inf error vs number of dofs');
