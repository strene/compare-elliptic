%% Example 1: Fundamental solution
% In this example, we look at grid orientation effects for different
% discretizations
mrstModule add upr                                                    % Generate PEBI grids
mrstModule add incomp mpfa mimetic vem vemmech                        % Incompressible solvers
mrstModule add ad-core ad-props ad-blackoil blackoil-sequential ntpfa % Nonlinear TPFA (requires AD)

%% Define anisotropic permeability
% We consider a constant permeability field with anisotropy ratio 5:1,
% rotated by an angle $\pi/8$

theta = pi/8;                  % Rotation angle
R  = [cos(theta), -sin(theta); ...
      sin(theta), cos(theta)]; % Rotation matrix
K0 = diag([5,1]);              % Rotate anisotropic permeability tensor
K  = R*K0*R';

%% Generate grids
% We generate three grids: A regular Cartesian grid, a Cartesian grid
% rotated so that it aligns with the principal axes of the permeability
% tensor, and an unstructured PEBI grid. In the paper, we show the example
% with a 51x51 grid resolution. Herein, we reduce this to 21x21 to reduce
% the runtime. To get identical results, you only need to set nc=51.

nc     = 21;                       % Number of cells in each axis direction
Gc     = cartGrid([nc,nc], [1,1]); % Cartesian grid
Gr     = alignedGrid(Gc,theta);    % Rotated Cartesian grid
Gp     = pebiGrid(1/nc, [1,1]);    % PEBI grid
grids  = {Gc, Gr, Gp};
ng     = numel(grids);

% Compute geometry
grids = cellfun(@(G) computeVEMGeometry(G), grids, 'uniformOutput', false);
% Names for plotting
gridNames = {'Cartesian', 'Rotated Cartesian', 'PEBI'};
% Plot the grids
figure('Position', [0,0,1000,300]);
for gNo = 1:numel(grids)
    subplot(1, 3, gNo), plotGrid(grids{gNo}); axis equal tight
    title(gridNames{gNo});
end

%% Define funcamental solution
% The fundamental solution to the Poisson equation
% $-\nabla \cdot K \nabla p = \delta(x)$
% can be found by a change of coordiantes so that the permeability is
% isotropic
[P,L] = eig(K);
p     = @(x) -1/(2*pi*sqrt(prod(diag(L)))).*log(sqrt(sum((x*(P/sqrt(L))).^2,2)));

%% Define boundary conditions and source
% Next we set the boundary conditions equal to the fundamental solution,
% and prescribe a point source at the center of the domain

[bcs, bcsVEM, sources, models] = deal(cell(numel(grids),1));
fluid = initSingleFluid('rho', 1, 'mu', 1); % Mandatory fluid structure

for gNo = 1:numel(grids)
    
    G = grids{gNo};
    % Transfor grid so that center is in [0,0]
    [~, c] = min(pdist2(G.cells.centroids, [1,1]*0.5));
    G.nodes.coords = G.nodes.coords - G.cells.centroids(c,:);
    % Recompute geometry
    G = computeVEMGeometry(G);
    % Assign boundary conditions
    faces       = boundaryFaces(grids{gNo});
    bcs{gNo}    = addBC([], faces, 'pressure', p(G.faces.centroids(faces,:)));
    % VEM can take function handles as BCs
    bcsVEM{gNo} = addBCVEM([], faces, 'pressure', p);
    % Add source in the center
    sources{gNo} = addSource([], c, 1);
    % Define the rock structure
    rock = makeRock(grids{gNo}, repmat(K([1,2,4]), G.cells.num, 1), 1);
    % For convenience, we use WaterModel from ad-blackoil to store grid,
    % rock and fluid information
    models{gNo} = WaterModel(G, rock, fluid);
    grids{gNo}  = G;
    
end

%% Set up solvers
% We will use TPFA, MPFA, MFD (mimetic), 1st- and 2nd-order VEM, and NTPFA
% to solve the problem. We define them compactly as a cell array of
% function handles
solvers = {
    @(state0, model, bc, src) ...
    incompTPFA(state0, model.G, computeTrans(model.G, model.rock), model.fluid, ...
               'bc', bc, 'src', src, 'matrixOutput', true)                    , ...
    @(state0, model, bc, src) ...
    incompSinglePhaseNTPFA(PressureOilWaterModelNTPFA(model.G, model.rock, [], ...
              'incTolPressure', 1e-14), 'bc', bc, 'src', src)                  ...
    @(state0, model, bc, src) ...
    incompMPFA(state0, model.G, computeMultiPointTrans(model.G, model.rock), model.fluid, ...
               'bc', bc, 'src', src, 'matrixOutput', true)                              , ...
    @(state0, model, bc, src) ...
    incompMimetic(state0, model.G, computeMimeticIP(model.G, model.rock), model.fluid, ...
                  'bc', bc, 'src', src, 'matrixOutput', true)                        , ...
    @(state0, model, bc, src) ...
    incompVEM(state0, model.G, computeVirtualIP(model.G, model.rock, 1), model.fluid, ...
              'bc', bc, 'src', src, 'cellPressure', true, 'matrixOutput', true)     , ...
    @(state0, model, bc, src) ...
    incompVEM(state0, model.G, computeVirtualIP(model.G, model.rock, 2), model.fluid, ...
              'bc', bc, 'src', src, 'matrixOutput', true)                           , ...
};
% Names for plotting
solverNames = {'TPFA', 'NTPFA', 'MPFA', 'MFD', 'VEM1', 'VEM2'};
ns          = numel(solvers);

%% Solve the problem
% We can now solve the problem using the different grids and solvers
states = cell(ng, ns);
for gNo = 1:ng
    model = models{gNo};                % Get model
    state0 = initResSol(model.G, 1, 1); % Define initial state
    % Solve problems (BCs given as function handle for VEM)
    for sNo = 1:ns
        fprintf('Solving %s with %s grid\n', solverNames{sNo}, gridNames{gNo});
        if any(strcmpi(solverNames{sNo}, {'VEM1', 'VEM2'}))
            states{gNo, sNo} = solvers{sNo}(state0, model, bcsVEM{gNo}, sources{gNo});
        else
            states{gNo, sNo} = solvers{sNo}(state0, model, bcs{gNo}, sources{gNo});
        end
    end
end

%% Plot the results
% We now inspect the results. As expected, the TPFA method, which is not
% consistent when the grid is not K-orthogonal, differes from all the 
close all
figSol = figure('Position', [0,0,1500,900], 'Name', 'Solutions'); colormap(jet)
figErr = figure('Position', [0,0,1500,900], 'Name', 'Error'    ); colormap(jet)
for sNo=1:ns, fprintf(1,'%-8s\t', solverNames{sNo}); end, fprintf(1,'\n');
for gNo = 1:ng
    pv = poreVolume(grids{gNo}, models{gNo}.rock);
    for sNo = 1:ns
        % Get solution
        pressure = states{gNo, sNo}.pressure;
        
        % Plot solution
        figure(figSol), subplot(ng, ns, sNo + ns*(gNo-1));
        plotCellData(grids{gNo}, pressure); axis equal tight
        title([solverNames{sNo}, ', ', gridNames{gNo}]);
        caxis([0,0.25]);
        
        % Plot error
        figure(figErr), subplot(ng, ns, sNo + ns*(gNo-1));
        discrp = abs(pressure - p(grids{gNo}.cells.centroids));
        plotCellData(grids{gNo}, discrp); axis equal tight
        title([solverNames{sNo}, ', ', gridNames{gNo}]);
        caxis([0,0.05]);
        
        % Output L1 error (remove source cell, which may have inifinite
        % true solution)
        discrp(isinf(discrp)) = 0;
        fprintf(1,'%.2e\t', sum((discrp.*pv).^2)./sum(pv));
    end
    fprintf(1,'\n');
end