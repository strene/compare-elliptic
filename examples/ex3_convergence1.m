%% Example 3.1: Convergence
% In this example, we look at the convergence properties for the different
% methods for a smooth solution
close all
mrstModule add upr                                                    % Generate PEBI grids
mrstModule add incomp mpfa mimetic vem vemmech                        % Incompressible solvers
mrstModule add ad-core ad-props ad-blackoil blackoil-sequential ntpfa % Nonlinear TPFA (requires AD)

%% Generate grids
% We consider two different grids: A regular Cartesian grid, and an
% unstructured PEBI grid
Gc = @(nc) computeGeometry(cartGrid([nc,nc], [1,1])); % Cartesian grid
Gp = @(nc) computeGeometry(pebiGrid(1/nc, [1,1]));    % PEBI grid
% Names for plotting
gridNames = {'Cartesian', 'PEBI'};

%% Define analytical solution
% We use a common trick where we construct an analytical solution to
% $\nalba \cdot K \nabla p = 0$
% as the real part of a function of the complex variable $x + i\sqrt{K_xx/K_yy} y$
% $exp(2*\pi(x + i\sqrt{K_xx/K_yy} y)
Kxx = 10; Kyy = 1;
p   = @(x) (exp(2*pi*x(:,1)).*cos(2*pi*sqrt(Kxx/Kyy).*x(:,2)))/(exp(2*pi)) + 1;

%% Plot the analytical solution
figure();
G = Gp(30);
G.nodes.coords = G.nodes.coords - [1,1]*0.5; G = computeGeometry(G);
unstructuredCarpetPlot(G, p(G.nodes.coords), 'nodeval', true); view(3);

%% Set up solvers
% We will use TPFA, MPFA, MFD (mimetic), 1st- and 2nd-order VEM, and NTPFA
% to solve the problem. We define them compactly as a cell array of
% function handles
fluid = initSingleFluid('mu', 1, 'rho', 1); % Mandatory
solvers = {
    @(state0, G, rock, bc) ...
    incompTPFA(state0, G, computeTrans(G, rock), fluid, ...
               'bc', bc, 'matrixOutput', true);
    @(state0, G, rock, bc) ...
    incompSinglePhaseNTPFA(PressureOilWaterModelNTPFA(G, rock, [], 'incTolPressure', 1e-14), 'bc', bc);
    @(state0, G, rock, bc) ...
    incompMPFA(state0, G, computeMultiPointTrans(G, rock), fluid, ...
               'bc', bc, 'matrixOutput', true);
    @(state0, G, rock, bc) ...
    incompMimetic(state0, G, computeMimeticIP(G, rock), fluid, ...
                  'bc', bc, 'matrixOutput', true);
    @(state0, G, rock, bc) ...
    incompVEM(state0, G, computeVirtualIP(G, rock, 1), fluid, ...
              'bc', bc, 'cellPressure', true, 'matrixOutput', true);
    @(state0, G, rock, bc) ...
    incompVEM(state0, G, computeVirtualIP(G, rock, 2), fluid, ...
              'bc', bc, 'cellPressure', true, 'matrixOutput', true);
};
% Names for plotting
solverNames = {'TPFA', 'NTPFA', 'MPFA', 'MFD', 'VEM1', 'VEM2'};
ns          = numel(solvers);

%% Solve problem on increasingly finer grids
% We investigate the convergence by solving the problem on successively
% finer grids. Tou can switch between Cartesian and PEBI grids below.
grid = 'PEBI'; % Either 'PEBI' or 'Cart'
nCells = [10, 20, 40];   % Number of cells for different resolutions
ng     = numel(nCells);
states              = cell(ng,ns);
[h, nDof]           = deal(zeros(ng,1));
[errorL2, errorInf] = deal(zeros(ng,ns));
for gNo = 1:ng
    switch grid
        case 'PEBI'
            G = Gp(nCells(gNo));
        case 'Cart'
            G = Gc(nCells(gNo));
    end
    G.nodes.coords = G.nodes.coords - [0.5, 0.5]; % Transfor grid so that center is in [0,0]
    G         = computeVEMGeometry(G);      % Compute geometry
    faces     = boundaryFaces(G);           % Get coundary faces
    rock      = makeRock(G, [Kxx, Kyy], 1); % Define the rock structure
    state0    = initResSol(G, 1, 1);        % Define initial state
    h(gNo)    = mean(G.cells.diameters);    % Store mean cell diameter
    for sNo = 1:ns
        fprintf('Solving %s on %s mesh with %d cells\n', ...
                 solverNames{sNo}, grid, G.cells.num);
        % Assign BCs (VEM can take function handle as BC)
        if any(strcmpi(solverNames{sNo}, {'VEM1', 'VEM2'}))
            bc = addBCVEM([], faces, 'pressure', p);
        else
            bc = addBC([], faces, 'pressure', p(G.faces.centroids(faces,:)));
        end
        states{gNo, sNo} = solvers{sNo}(state0, G, rock, bc); % Compute solution
        nDof(gNo, sNo)   = G.cells.num; % Store nDof
        if isfield(states{gNo, sNo}, 'A')
            nDof(gNo,sNo) = size(states{gNo, sNo}.A, 1);
        end
        % Compute error norm and number of dofs
        if any(strcmpi(solverNames{sNo}, {'VEM1', 'VEM2'}))
            % VEM does has node pressure as dof, so we use this
            errorL2(gNo, sNo)  = norm(states{gNo, sNo}.nodePressure - p(G.nodes.coords));
            errorInf(gNo, sNo) = norm(states{gNo, sNo}.nodePressure - p(G.nodes.coords), 'inf');
        else
            errorL2(gNo, sNo)  = norm(states{gNo, sNo}.pressure - p(G.cells.centroids));
            errorInf(gNo, sNo) = norm(states{gNo, sNo}.pressure - p(G.cells.centroids), 'inf');
        end
    end

end

%% Plot convergence
% We investigate convergence by log-log-plots of L2 error vs mean cell
% diameter
figure(), plotConvergence(errorL2, h, solverNames, [2,3]);
title('L2 error vs mean cell diameter');
% ... and Linf error vs number of degrees of freedom
figure(), plotConvergence(errorInf, nDof, solverNames, [2,3], 'dofs', true);
title('Inf error vs number of dofs');
