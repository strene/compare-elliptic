%% Example 4: Near-well simulation
% In this example, we consider simulation of the near-well pressure around
% a well with two intersecting fractures. Notice that this example consumes
% a lot of memory for the VEM2 computation, and you will typically need to
% have 32 GiB memory to run the full example.
mrstModule add upr                                                    % Generate PEBI grids
mrstModule add incomp mpfa mimetic vem vemmech                        % Incompressible solvers
mrstModule add ad-core ad-props ad-blackoil blackoil-sequential ntpfa % Nonlinear TPFA (requires AD)

%% Construct 2D grid
% We want to construct a domain that looks like a typical cell in a
% stratigraphic grid, with a well perforating the centroid, and two
% fractures propagating from it. We start from a 2D PEBI grid

well = [88,90]; % Well coordinates
% Fracture lines
fractures = {[ 61,  50;  61,  61;  65,  71;  70,  78;    well; 98, 98; ...
              104, 104; 107, 108; 109, 112; 111, 120; 112, 131]      , ...
             [105, 39;      well;  77, 146]                          };
% Boundary
bdr       = [5, 8; 147, 13; 186, 197; 23, 155];

% Construct 2D PEBI grid with refinement around the fractures
G2D = pebiGrid(155/sqrt(50), [1,1]        , ...
               'polyBdr'       , bdr      , ... % Boundary
               'wellLines'     , fractures, ... % Fractures as volumetric objects
               'wellRefinement', true     , ... % Use refinement
               'wellGridFactor', 0.1      , ... % Fracture cells size relative to grid cell size
               'protLayer'     , true     , ... % Protection layer around fracutures
               'wellEps'       , 0.08*155);     % Degree of refinement
G2D = computeGeometry(G2D);

% Find well cell
[~, c] = min(pdist2(G2D.cells.centroids, well));
G2D.cells.tag = G2D.cells.tag*1; % Use G.cells.tag to mark fracture cells
G2D.cells.tag(c) = 2;            % ... and well cell

%% Make 3D grid and assign petrophysical properties
% We use the 2D grid to make a 2.5D PEBI grid, and then assign
% petrophysical properties to it
H  = 15*meter; % Grid height
nl = 5;        % Number of layers
G3D = makeLayeredGrid(G2D, (H/nl)*ones(nl,1));
G3D = computeGeometry(G3D);
G3D.cells.tag = repmat(G2D.cells.tag, nl, 1); % Map cells from 2D grid
% Make lognormal permeability with distinct mean in each layer
rng(1);
n = [50,50,5];
K = logNormLayers(n)*milli*darcy;
K = reshape(K, n);
% Sample to grid
perm = sampleFromBox(G3D, K);
% Rotate by $\pi/6$
K = [3,1,1];
theta = pi/6;
R  = [cos(theta), -sin(theta), 0, ; sin(theta), cos(theta), 0; 0 0 1];
K0 = diag(K);
K  = R*K0*R';
perm = perm.*K([1,2,3,5,6,9]);
rock = makeRock(G3D, perm, 0.4);
% Assign high permeability (1 d) to fractures
ix = G3D.cells.tag > 0;
rock.perm(ix,:) = repmat([1, 0, 0, 1, 0, 1]*1*darcy, nnz(ix), 1);

%% Shift vertical coordinates
% Finally, we shift the veritcal coordinates to make the PEBI grid look
% like a skewed, stratigraphic hexahedral grid cell
% Bilinear functions definig the corners 
phi = @(x) [(1-x(:,1)).*(1-x(:,2)), ...
            x(:,1).*(1-x(:,2))    , ...
            x(:,1).*x(:,2)        , ...
            (1-x(:,1)).*x(:,2)    ];
x2X = @(x) phi(x)*bdr;
% Make a scattered interpolant to map the vertical coordinates acording to
% a bilinear function defined by the corner height
[x,y] = ndgrid(linspace(-1,1,10));
x = [x(:), y(:)]; X = x2X(x);
X2x1 = scatteredInterpolant(X(:,1), X(:,2), x(:,1));
X2x2 = scatteredInterpolant(X(:,1), X(:,2), x(:,2));
X2x = @(X) [X2x1(X(:,1), X(:,2)), X2x2(X(:,1), X(:,2))];
xy  = @(x) prod(x,2);
xy2 = @(x) x(:,1).*(1-x(:,2));
x = G3D.nodes.coords;
x(:,3) = x(:,3) - xy(X2x(x(:,[1,2])))*50*meter - xy2(X2x(x(:,[1,2])))*10*meter;
% Assing grid coordinates and recomput geometry
G3D.nodes.coords = x;
G3D = computeVEMGeometry(G3D);

%% Plot the resulting grid
figure('Position', [0,0, 1000, 400])

subplot(1,2,1); % Plot permeability
plotCellData(G3D, log10(rock.perm(:,1)), 'edgealpha', 0.2);
axis equal tight, box on, view(3)

subplot(1,2,2); % Plot fractures and well cell
plotGrid(G3D, 'facecolor', 'none');
plotGrid(G3D, G3D.cells.tag == 1, 'facecolor', 'k');
plotGrid(G3D, G3D.cells.tag == 2, 'facecolor', 'r');
axis equal tight, box on, view(3)

%% Initialize fluid
% We simulate injection of water into oil
% Fluid for incomp solvers
fluid = initSimpleFluid('n'  , [2,2]                       , ...
                        'mu' , [0.5,1]*centi*poise         , ...
                        'rho', [1000, 800]*kilogram/meter^3);
% Fluid for NTPFA (requires AD)
fluidADI = initSimpleADIFluid('phases', 'WO'                         , ...
                              'n'     , [2,2]                        , ...
                              'mu'    , [0.5,1]*centi*poise          , ...
                              'rho'   , [1000, 800]*kilogram/meter^3);

%% Assign boundary conditions

pRes = 250*barsa; % Initial reservoir pressure
time = 0.1*year;  % Injection of one PV over 0.1 years
rate = sum(poreVolume(G3D, rock))/time;
wc   = find(G3D.cells.tag == 2);
% Treat each perforated cell as source
src = addSource([], wc, rate/numel(wc), 'sat', [1,0]);
% Fint vertical boundary faces
bf  = boundaryFaces(G3D);
ix  = G3D.faces.normals(bf,3)./G3D.faces.areas(bf) < 1e-3;
sideFaces = bf(ix);
% Assign BC
bc = addBC([], sideFaces, 'pressure', pRes, 'sat', [1,0]);

%% Set up solvers
% We will use TPFA, MPFA, MFD (mimetic), 1st- and 2nd-order VEM, and NTPFA
% to solve the problem. We define them compactly as a cell array of
% function handles
solvers = {
    @(state0) ...
    incompTPFA(state0, G3D, computeTrans(G3D, rock), fluid, 'bc', bc, 'src', src, 'matrixoutput', true), ...
    @(state0) ...
    incompSinglePhaseNTPFA(PressureOilWaterModelNTPFA(G3D, rock, fluidADI, 'incTolPressure', 1e-14), 'bc', bc, 'src', src), ...
    @(state0) ...
    incompMPFA(state0, G3D, computeMultiPointTrans(G3D, rock), fluid, 'bc', bc, 'src', src, 'matrixoutput', true), ...
    @(state0) ...
    incompMimetic(state0, G3D, computeMimeticIP(G3D, rock), fluid, 'bc', bc, 'src', src, 'matrixoutput', true), ...
    @(state0) ...
    incompVEM(state0, G3D, computeVirtualIP(G3D, rock, 1), fluid, 'bc', bc, 'src', src, 'cellPressure', true, 'matrixoutput', true), ...
    @(state0) ...
    incompVEM(state0, G3D, computeVirtualIP(G3D, rock, 2), fluid, 'bc', bc, 'src', src, 'matrixoutput', true) ...
};
% Names for plotting
sNames = {'TPFA', 'NTPFA', 'MPFA', 'MFD', 'VEM1', 'VEM2'};
ns     = numel(solvers);

%% Simulate problem
state0 = initResSol(G3D, pRes, [0,1]);
states = cell(ns,1);
for sNo = 1:ns
    fprintf('Solving %s ... ', sNames{sNo})
    states{sNo} = solvers{sNo}(state0);
    if any(strcmpi(sNames{sNo}, {'VEM1', 'VEM2'}))
        states{sNo} = conserveFlux(states{sNo}, G3D, rock);
    end
    fprintf('Done \n')
end

%% Plot results
% We plot the resulting pressure computed using each solver
figure('Position', [0,0,1000,500]);
pmax = max(cellfun(@(s) max(s.pressure), states));
pmin = min(cellfun(@(s) min(s.pressure), states));
for sNo = 1:numel(states)
    subplot(2,3,sNo)
    plotCellData(G3D, states{sNo}.pressure, 'edgec', 'none');
    caxis([pmin, pmax*0.8]);
    axis equal tight, box on, view(3);
    title(solverNames{sNo});
end

%% Plot fraction of flux out of each vertical side
% Finally, we look at the fraction of the total flux out of the domain that
% flows out of the four vertical sides
figure('Position', [0,0,1000,500]);
% Identify faces
sgn   = 1 - 2*(G3D.faces.neighbors(sideFaces,2) ~= 0);
n     = G3D.faces.normals(sideFaces,:)./G3D.faces.areas(sideFaces).*sgn;
west  = n(:,1) < -0.9;
east  = n(:,1) > 0.9;
south = n(:,2) < -0.9;
north = n(:,2) > 0.9;
compass = [south, east, north, west];
% Plot faces
subplot(2,4,2:3); clr = lines(4);
plotGrid(G3D, 'facec', 'none', 'edgealpha', 0.2);
plotGrid(G3D, G3D.cells.tag == 1, 'facec', 'k');
plotGrid(G3D, G3D.cells.tag == 2, 'facec', 'r');
for i = 1:size(compass,2)
    plotFaces(G3D, sideFaces(compass(:,i)), clr(i,:));
end
axis equal tight, box on, view(3);
% Compute flux fraction
flux = zeros(numel(states), size(compass,2));
for sNo = 1:numel(states)
    f = states{sNo}.flux(sideFaces).*sgn;
    for i = 1:size(compass,2)
        flux(sNo,i) = sum(f(compass(:,i)))./sum(f);
    end
end
% Plot flux fractions
fNames = {'South', 'East', 'North', 'West'};
for i = 1:size(compass,2)
    subplot(2,4,4+i)
    b = bar(flux(:,i)*100);
    b.FaceColor = clr(i,:);
    ax = gca;
    ax.XTickLabel         = sNames;
    ax.XTickLabelRotation = -90;
    title(fNames{i});
end