%% Example: Multiphase flow
% In this example, we set up the multiphase simulation on a subset of the
% SAIGUP model, as discussed in Section 4 of the paper. Notice that the
% simulation takes long time to run, in particular for the VEM2 method.
close all
mrstModule add incomp mpfa mimetic vem vemmech                        % Incompressible solvers
mrstModule add ad-core ad-props ad-blackoil blackoil-sequential ntpfa % Nonlinear TPFA (requires AD)

%% Load the SAIGUP model
% This is stored in GRDECL formal
grdecl = fullfile(getDatasetPath('SAIGUP'), 'SAIGUP.GRDECL');
grdecl = readGRDECL(grdecl);
usys   = getUnitSystem('METRIC');
grdecl = convertInputUnits(grdecl, usys);
% Convert the .GRDECL into MRST grid format
GFull    = processGRDECL(grdecl);
% Get the rock structure
rockFull = grdecl2Rock(grdecl, GFull.cells.indexMap);

%% Get subset
% We will only simulate on a small subset of the full model, which we
% extract from the full model using the logical Cartesian coordinates
ijk  = gridLogicalIndices(GFull);
slab = [Inf, 15, 10];
keep = true(GFull.cells.num,1);
for dNo = 1:3
    keep = keep & ijk{dNo} <= slab(dNo);
end
G = extractSubgrid(GFull, keep); % Extract the subgrid
G = computeVEMGeometry(G);
rock = makeRock(G, rockFull.perm(keep,:), rockFull.poro(keep)); % ... and rock

%% Plot the full model and the subset
figure('Position', [0, 0, 1500, 500])
subplot(1,2,1); % Plot the full model
plotGrid(GFull, 'edgeColor', 'none'); plotGrid(G);
pbaspect([1,2,0.5]); view(3); light('Position', [0,0,-1]); title('Full model');
subplot(1,2,2); % Plot the subset with permeability
plotCellData(G, log10(rock.perm(:,1)));
pbaspect([2,1,1]); view(3); light('Position', [0,0,-1]); title('Subset');

%% Set up fluid
% We simulate injection of water into oil. Both phases have Brooks-Corey
% relative permeabilities with exponent 2.
fluid = initSimpleFluid('n'  , [2, 2]                      , ...
                        'rho', [1000, 800]*kilogram/meter^3, ...
                        'mu' , [0.5, 1]*centi*poise        );
                                
%% Assing boundary conditions
% We simulate water injection by assigning a pressure drop
bc = [];
% Get boundary cells and faces
[faces, cells] = boundaryFaces(G);
xf  = G.faces.centroids(faces,:);
tol = 20;
% Inlet through faces with x coord = 0
inlet   = abs(xf(:,1)) < tol;
% Outlet through an opposite corner
outlet1 = abs(xf(:,1)-1950) < tol & xf(:,2) > 1050;
% Assign boundary conditions
bc = addBC(bc, faces(inlet)  , 'pressure', 800*barsa, 'sat', [1,0]);
bc = addBC(bc, faces(outlet1), 'pressure', 50*barsa , 'sat', [0,1]);
% Assign initial state
state0 = initResSol(G, 50*barsa, [0,1]);
% We simulate 2 years of injection with timestes that geometrically ramps
% up to 15 days
timesteps = rampupTimesteps(2*year, 15*day);
schedule = simpleSchedule(timesteps, 'bc', bc);

%% Set up solvers
% We will use TPFA, MPFA, MFD (mimetic), 1st- and 2nd-order VEM, and NTPFA
% to solve the problem. We define them compactly as a cell array of
% function handles
solvers = {
    @(state0, fluid, src, bc, W) ...
    incompTPFA(state0, G, computeTrans(G, rock), fluid, ...
               'bc', bc, 'matrixOutput', true);
    @(state0, fluid, src, bc, W) ...
    incompMimetic(state0, G, computeMimeticIP(G, rock), fluid, ...
                  'bc', bc, 'matrixOutput', true);
    @(state0, fluid, src, bc, W) ...
    incompVEM(state0, G, computeVirtualIP(G, rock, 1, 'invertBlocks', 'MEX'), fluid, ...
              'bc', bc, 'cellPressure', true, 'matrixOutput', true);
    @(state0, fluid, src, bc, W) ...
    incompVEM(state0, G, computeVirtualIP(G, rock, 2, 'invertBlocks', 'MEX'), fluid, ...
              'bc', bc, 'cellPressure', true, 'matrixOutput', true);
};
% Names for plotting
solverNames = {'TPFA', 'NTPFA', 'MPFA', 'MFD', 'VEM1', 'VEM2'};
ns          = numel(solvers);

%% Simulate injection
% We simulate injection with the function simulateScheduleIncomp, which has
% a similar as simulateScheduleAD
% We must conserve the flux for the VEM solvers
consFlux = false(ns,1); consFlux(end-1:end) = true;
states   = cell(ns, 1);
% NOTE: The VEM2 solutions takes a long time to compute
ix = 1:3;
for sNo = ix
    disp(solverNames{sNo})
    [ws, states{sNo}, rep] = simulateScheduleIncomp(state0, G, rock, fluid, schedule, ...
            'pressureSolver', solvers{sNo}, 'conserveFlux', consFlux(sNo));
end

%% Compute flow across the inlet/outlet
tvec = cumsum(schedule.step.val)/day;
faces = boundaryFaces(G);
bcFaces = {faces(inlet), faces(outlet1)};
[wcut, p] = deal(cell(numel(solverNames), 1));
src = schedule.control(1).src;
for sNo = ix
    st = states{sNo};
    for tNo = 1:numel(st)
        for fNo = 1:numel(bcFaces)
            faces = bcFaces{fNo};
            cells = sum(G.faces.neighbors(faces,:),2);
            wcut{sNo}(tNo,fNo) = mean(st{tNo}.s(cells,1));
            p{sNo}(tNo,fNo) = mean(st{tNo}.pressure(cells));
        end
    end
end
figure, 
subplot(2,2,1), hold all
for sNo=ix, plot(tvec, wcut{sNo}(:,1),'LineWidth',2); end
legend(solverNames(ix),'Location', 'best'); title('Water cut at inlet');

subplot(2,2,2), hold all
for sNo=ix, plot(tvec, wcut{sNo}(:,2),'LineWidth',2); end
legend(solverNames(ix),'Location', 'best'); title('Water cut at outlet');

subplot(2,2,3), hold all
for sNo=ix, plot(tvec, p{sNo}(:,1)/barsa,'LineWidth',2); end
legend(solverNames(ix),'Location', 'best'); title('Mean pressure at inlet');

subplot(2,2,4), hold all
for sNo=ix, plot(tvec, p{sNo}(:,2)/barsa,'LineWidth',2); end
legend(solverNames(ix),'Location', 'best'); title('Mean pressure at outlet');


%% Get condition numbers
figure, hold all
for sNo = ix
    cnd = zeros(numel(tvec),1);
    for tNo = 1:numel(tvec)
        tic
        fprintf('Estimating %s condition number %d ...', solverNames{sNo}, tNo)
        cnd(tNo) = condest(states{sNo}{tNo}.A);
        toc
    end
    plot(tvec, cnd,'-o','LineWidth',2,'MarkerSize',4);
end
set(gca,'Yscale','log');
legend(solverNames(ix));set(gca,'Yscale','log');